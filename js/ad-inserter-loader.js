(($, drupalSettings) => {
  const mobileBreakpoint = drupalSettings['ad_inserter']['mobile_breakpoint'];
  const windowWidth = $(window).width();
  Drupal.behaviors.ad_inserter = {
    attach: function (context, settings) {
      once('ad-inserter', '.ad-inserter-block', context).forEach(
        function (element) {
          let adInserterBlock = $(element);
          let screen = adInserterBlock.data('ad-inserter-screen');
          let hideScreen = windowWidth <= mobileBreakpoint ? 'mobile' : 'desktop';
          if (screen !== hideScreen && screen !== 'all') {
            adInserterBlock.remove();
            return;
          }

          if (screen !== 'all') {
            let bodyAdInserter = adInserterBlock.find('.ad-inserter--body');
            // Decode the Base64 string
            let decodedString = atob(bodyAdInserter.html(), 'base64');
            bodyAdInserter.html(decodedString);
          }

          adInserterBlock.removeClass('hidden');
        },
      );
    },
  };
})(jQuery, drupalSettings);
