<?php

namespace Drupal\ad_inserter;

use Drupal\ad_inserter\Entity\AdInserter;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler for Ad Inserter entities.
 *
 * This class extends the default SQL storage handler for content entities and
 * provides a custom method to load an Ad Inserter entity by its machine name.
 *
 * @see \Drupal\Core\Entity\Sql\SqlContentEntityStorage
 * @see \Drupal\ad_inserter\Entity\AdInserter
 */
class AdInserterStorage extends SqlContentEntityStorage implements ContentEntityStorageInterface {

  /**
   * Loads an Ad Inserter entity by its machine name.
   *
   * @param string $machine_name
   *   The machine name of the Ad Inserter entity.
   *
   * @return \Drupal\ad_inserter\Entity\AdInserter|null
   *   The Ad Inserter entity object if found, or NULL if not.
   */
  public function loadByMachineName($machine_name): ?AdInserter {
    $entities = $this->loadByProperties(['machine_name' => $machine_name]);
    return $entities ? array_shift($entities) : NULL;
  }

}
