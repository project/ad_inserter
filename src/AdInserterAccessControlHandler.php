<?php

namespace Drupal\ad_inserter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the Ad Inserter entity.
 *
 * Provides access control for viewing, updating, and deleting entities.
 */
class AdInserterAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // Administrators can view/update/delete all user profiles.
    if ($account->hasPermission('administer ad inserter')) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    // No opinion.
    return AccessResult::neutral();
  }

}
