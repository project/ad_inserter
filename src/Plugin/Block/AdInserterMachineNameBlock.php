<?php

namespace Drupal\ad_inserter\Plugin\Block;

use Drupal\ad_inserter\Entity\AdInserterInterface;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an 'Ad Inserter by machine name' Block.
 */
#[Block(
  id: "ad_inserter_machine_name",
  admin_label: new TranslatableMarkup("Ad Inserter by machine name")
)]
class AdInserterMachineNameBlock extends AdInserterBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'ad_inserter_machine_name' => NULL,
    ];
  }

  /**
   * Builds the block configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The modified form array.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $adInserterId = $this->configuration['ad_inserter_machine_name'];

    $form['ad_inserter_machine_name'] = [
      '#title' => t('Write machine name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $adInserterId,
    ];

    return $form;
  }

  /**
   * Validates the block configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $machineName = $form_state->getValue('ad_inserter_machine_name');
    $entity = $this->adInserterStorage->loadByMachineName($machineName);
    if (!$entity) {
      $form_state->setErrorByName('ad_inserter_machine_name', $this->t('The machine name does not exist.'));
    }
  }

  /**
   * Submits the block configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['ad_inserter_machine_name'] = $values['ad_inserter_machine_name'];
  }

  /**
   * Loads the Ad Inserter entity by its machine name.
   *
   * @return \Drupal\ad_inserter\Entity\AdInserterInterface|null
   *   The Ad Inserter entity if found, or NULL if not.
   */
  public function getAdInserter(): ?AdInserterInterface {
    $adInserterMachineName = $this->configuration['ad_inserter_machine_name'];
    if (!$adInserterMachineName) {
      return NULL;
    }
    /** @var \Drupal\ad_inserter\Entity\AdInserter $adInserter */
    $this->adInserter = $this->adInserterStorage->loadByMachineName($adInserterMachineName);
    return $this->adInserter;
  }

}
