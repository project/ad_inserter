<?php

namespace Drupal\ad_inserter\Plugin\Block;

use Drupal\ad_inserter\AdInserterStorage;
use Drupal\ad_inserter\Entity\AdInserter;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for Ad Inserter blocks.
 *
 * This abstract class handles common functionalities for Ad Inserter blocks.
 */
abstract class AdInserterBaseBlock extends BlockBase implements ContainerFactoryPluginInterface, AdInserterBaseBlockInterface {

  /**
   * The Ad Inserter entity.
   *
   * @var \Drupal\ad_inserter\Entity\AdInserter|null
   */
  protected ?AdInserter $adInserter = NULL;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Ad Inserter storage service.
   *
   * @var \Drupal\ad_inserter\AdInserterStorage
   */
  protected AdInserterStorage $adInserterStorage;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs an AdInserterBaseBlock object.
   *
   * @param array $configuration
   *   Configuration settings for the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->adInserterStorage = $entityTypeManager->getStorage('ad_inserter');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Creates an instance of the block.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array $configuration
   *   Configuration settings for the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return static
   *   A new instance of the block.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $this->getAdInserter();
    // Ensure is active and exists.
    if (!$this->adInserter || !$this->adInserter->isActive()) {
      return NULL;
    }
    $options = [
      'status' => TRUE,
    ];
    // Alter option to show.
    $this->moduleHandler->invokeAll('ad_inserter_alter_status', [$this->adInserter, &$options]);

    if ($options['status'] === FALSE) {
      return NULL;
    }
    $build['#attributes']['class'][] = 'ad-inserter-block';
    // We always hide ad inserter before JS is not showing them.
    if ($this->adInserter->getScreen() != 'all') {
      $build['#attributes']['class'][] = 'hidden';
    }
    $build['#attributes']['data-ad-inserter-id'] = $this->adInserter->id();
    $build['#attributes']['data-ad-inserter-screen'] = $this->adInserter->getScreen();
    $machineName = $this->adInserter->getMachineName();
    if ($machineName) {
      $build['#attributes']['data-ad-inserter-machine-name'] = $machineName;
    }
    $build['#attached']['library'][] = 'ad_inserter/loader';
    $build['ad_inserter'] = $this->entityTypeManager->getViewBuilder('ad_inserter')->view($this->adInserter);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $this->getAdInserter();
    if ($this->adInserter) {
      return Cache::mergeTags(parent::getCacheTags(), $this->adInserter->getCacheTags());
    }
    return parent::getCacheTags();
  }

}
