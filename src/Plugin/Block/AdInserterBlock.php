<?php

namespace Drupal\ad_inserter\Plugin\Block;

use Drupal\ad_inserter\Entity\AdInserterInterface;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a 'Ad Inserter' Block.
 */
#[Block(
  id: "ad_inserter",
  admin_label: new TranslatableMarkup("Ad Inserter")
)]
class AdInserterBlock extends AdInserterBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'ad_inserter_id' => NULL,
    ];
  }

  /**
   * Builds the block configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The modified form array.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $adInserterId = $this->configuration['ad_inserter_id'];

    if ($adInserterId) {
      $this->adInserter = $this->adInserterStorage->load($adInserterId);
    }
    $form['ad_inserter_id'] = [
      '#title' => t('Select ad inserter to display'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'ad_inserter',
      '#default_value' => $this->adInserter,
    ];

    return $form;
  }

  /**
   * Submits the block configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['ad_inserter_id'] = $values['ad_inserter_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAdInserter(): ?AdInserterInterface {
    if ($this->adInserter) {
      return $this->adInserter;
    }
    $adInserterId = $this->configuration['ad_inserter_id'];
    if (!$adInserterId) {
      return NULL;
    }
    /** @var \Drupal\ad_inserter\Entity\AdInserter $adInserter */
    $this->adInserter = $this->adInserterStorage->load($adInserterId);
    return $this->adInserter;
  }

}
