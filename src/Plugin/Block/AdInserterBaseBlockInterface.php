<?php

namespace Drupal\ad_inserter\Plugin\Block;

use Drupal\ad_inserter\Entity\AdInserterInterface;

/**
 * Defines an interface for Ad Inserter base blocks.
 *
 * This interface provides a method for retrieving the Ad Inserter entity
 * associated with the block.
 */
interface AdInserterBaseBlockInterface {

  /**
   * Gets the Ad Inserter entity.
   *
   * @return \Drupal\ad_inserter\Entity\AdInserterInterface|null
   *   The Ad Inserter entity object if it exists, or NULL if not.
   */
  public function getAdInserter(): ?AdInserterInterface;

}
