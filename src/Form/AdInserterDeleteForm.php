<?php

namespace Drupal\ad_inserter\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ad Inserter entities.
 *
 * @ingroup ad_inserter
 */
class AdInserterDeleteForm extends ContentEntityDeleteForm {

}
