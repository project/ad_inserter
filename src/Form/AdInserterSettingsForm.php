<?php

namespace Drupal\ad_inserter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form for the Ad Inserter module.
 *
 * This form allows administrators to configure settings for the Ad Inserter.
 */
class AdInserterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ad_inserter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ad_inserter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [
      'mobile_breakpoint' => [
        '#type' => 'number',
        '#title' => $this->t('Mobile Breakpoint'),
        '#default_value' => $this->config('ad_inserter.settings')->get('mobile_breakpoint'),
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ad_inserter.settings')
      ->set('mobile_breakpoint', $form_state->getValue('mobile_breakpoint'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
