<?php

namespace Drupal\ad_inserter\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\StatusItem;
use Drupal\user\UserInterface;

/**
 * Defines the ad inserter entity class.
 *
 * @ContentEntityType(
 *   id = "ad_inserter",
 *   label = @Translation("Ad Inserter"),
 *   label_collection = @Translation("Ad Inserter"),
 *   label_singular = @Translation("Ad Inserter"),
 *   label_plural = @Translation("Ad Inserter"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Ad Inserter",
 *     plural = "@count Ad Inserter",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\ad_inserter\AdInserterAccessControlHandler",
 *     "storage" = "Drupal\ad_inserter\AdInserterStorage",
 *     "list_builder" = "Drupal\ad_inserter\AdInserterListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\ad_inserter\Form\AdInserterForm",
 *       "add" = "Drupal\ad_inserter\Form\AdInserterForm",
 *       "edit" = "Drupal\ad_inserter\Form\AdInserterForm",
 *       "delete" = "Drupal\ad_inserter\Form\AdInserterDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer ad inserter",
 *   base_table = "ad_inserter",
 *   data_table = "ad_inserter_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "name" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "published" = "status",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/ad-inserter/{ad_inserter}",
 *     "add-page" = "/ad-inserter/add",
 *     "edit-form" = "/ad-inserter/{ad_inserter}/edit",
 *     "delete-form" = "/ad-inserter/{ad_inserter}/delete",
 *     "collection" = "/admin/ad-inserter/list",
 *   },
 *   field_ui_base_route = "entity.ad_inserter.admin_form",
 *   common_reference_target = TRUE
 * )
 */
class AdInserter extends ContentEntityBase implements AdInserterInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool {
    return $this->get('status')->value == 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getBody(): ?string {
    return $this->get('body')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getScreen(): string {
    return $this->get('screen')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineName(): ?string {
    return $this->get('machine_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['id']->setLabel(t('Ad inserter ID'))
      ->setDescription(t('The user ID.'));

    $fields['uuid']->setDescription(t('The d inserter UUID.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Ad Inserter entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'region' => 'hidden',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of this Ad Inserter.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine Name'))
      ->setDescription(t('Not required. Uniq code for ad inserter, for rendering block by machine name. ex: ad_inserter_sidebar_top'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', FALSE);

    // The text of the contact message.
    $fields['body'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Message'))
      ->setDescription(t('Add HTML or script tag to be rendered within the body structure.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 12,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 0,
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['screen'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Screen'))
      ->setDescription(t('The screen of this Ad Inserter.'))
      ->setRequired(TRUE)
      ->setDefaultValue('all')
      ->setSettings([
        'allowed_values' => [
          'all' => 'All',
          'mobile' => 'Mobile',
          'desktop' => 'Desktop',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the ad inserter is active .'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', FALSE);
    $fields['status']->getItemDefinition()->setClass(StatusItem::class);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the user was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the user was last edited.'));

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    return $fields;
  }

}
