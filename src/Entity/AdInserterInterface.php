<?php

namespace Drupal\ad_inserter\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for Ad Inserter entities.
 *
 * This interface defines the methods for managing Ad Inserter entities.
 */
interface AdInserterInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Checks if the Ad Inserter is active.
   *
   * @return bool
   *   TRUE if the Ad Inserter is active, FALSE otherwise.
   */
  public function isActive(): bool;

  /**
   * Gets the body content of the Ad Inserter.
   *
   * @return string|null
   *   The body content of the Ad Inserter, or NULL if not set.
   */
  public function getBody(): ?string;

  /**
   * Gets the screen configuration for the Ad Inserter.
   *
   * @return string
   *   The screen configuration string.
   */
  public function getScreen(): string;

  /**
   * Gets the machine name of the Ad Inserter.
   *
   * @return string|null
   *   The machine name of the Ad Inserter, or NULL if not set.
   */
  public function getMachineName(): ?string;

}
