<?php

namespace Drupal\ad_inserter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Symfony\Contracts\Translation\TranslatorTrait;

/**
 * Provides a listing of Ad Inserter type entities.
 */
class AdInserterListBuilder extends EntityListBuilder {

  use TranslatorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => $this->t('ID'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['name'] = $this->t('Name');
    $header['uniq_name'] = $this->t('Uniq name');
    $header['screen'] = $this->t('Screen');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\ad_inserter\Entity\AdInserter $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.ad_inserter.canonical',
      ['ad_inserter' => $entity->id()]
    );
    $row['screen'] = $entity->getScreen();
    $row['status'] = $entity->isActive();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this
      ->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query
        ->pager($this->limit);
    }
    return $query
      ->execute();
  }

}
